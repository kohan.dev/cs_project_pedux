import { applyMiddleware, createStore } from 'redux';
import rootSaga from '../js/roots/rootSaga';
import rootReducer from '../js/roots/rootReducer';
import createSagaMiddleware from 'redux-saga';
import configCore from '../js/config/config';
import styles from '../js/themes/csTheme';
import Strings from '../js/strings/en';

const sagaMiddleware = createSagaMiddleware();

const initialState = {
    authData: {
        uuid: '',
        loginData: {
            user: 'admin',
            password: 'root',
            database: 'db_tables',
            table: 'person',
        },
        isLoggedSuccessfully: false,
        userPermissions: {
            read: false,
            write: false,
            create: false,
        },
        errorData: {
            isError: false,
            errorMessage: '',
        },
    },
    configData: {
        currentVersion: configCore.version,
        searchKey: configCore.searchValue,
    },
    themes: {
        theme: styles,
    },
    model: {
        currentId: 1,
        list: [],
        logs: [],
        person: {},
        setOfId: [],
        inputAdd: '',
        inputChange: '',
        changeKey: '',
        deleteKey: '',
    },
    userLocalSettings: {
        setOfVersions: ['variant1', 'variant2', 'variant3', 'variant4'],
        selectedTab: Strings.resources.editorTabName,
        selectEdit: 'List',
        // setOfCurrentKey: ['name', 'age', 'gender', 'address'],
        setOfCurrentKey: [],
    },
    modalWindowData: {
        showModal: false,
    },
};

global.store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(sagaMiddleware),
);

sagaMiddleware.run(rootSaga);
