import * as actions from '../actions';
import * as mockActions from '../../../testUtils/mockData/mockActions';

describe('Test modelActions', () => {
    it('changeAuthStateStore', () => {
        assert.deepEqual(actions.changeAuthStateStore({}), mockActions.changeAuthStateStore({}));
    });

    it('changeLoginDataStore', () => {
        assert.deepEqual(actions.changeLoginDataStore({}), mockActions.changeLoginDataStore({}));
    });

    it('setErrorStore', () => {
        assert.deepEqual(actions.setErrorStore({}), mockActions.setErrorStore({}));
    });

    it('setCurrentUuidStore', () => {
        assert.deepEqual(actions.setCurrentUuidStore({}), mockActions.setCurrentUuidStore({}));
    });

    it('setUserPermissionsStore', () => {
        assert.deepEqual(actions.setUserPermissionsStore({}), mockActions.setUserPermissionsStore({}));
    });

    it('changeCurrentVersionStore', () => {
        assert.deepEqual(actions.changeCurrentVersionStore({}), mockActions.changeCurrentVersionStore({}));
    });

    it('changeSelectedTabStore', () => {
        assert.deepEqual(actions.changeSelectedTabStore({}), mockActions.changeSelectedTabStore({}));
    });

    it('changeListOfPersons', () => {
        assert.deepEqual(actions.changeListOfPersons({}), mockActions.changeListOfPersons({}));
    });

    it('changeListOfPersonsStore', () => {
        assert.deepEqual(actions.changeListOfPersonsStore({}), mockActions.changeListOfPersonsStore({}));
    });

    it('changeLogs', () => {
        assert.deepEqual(actions.changeLogs({}), mockActions.changeLogs({}));
    });

    it('changeLogsStore', () => {
        assert.deepEqual(actions.changeLogsStore({}), mockActions.changeLogsStore({}));
    });

    it('setSelectEditStore', () => {
        assert.deepEqual(actions.setSelectEditStore({}), mockActions.setSelectEditStore({}));
    });

    it('setCurrentIdStore', () => {
        assert.deepEqual(actions.setCurrentIdStore({}), mockActions.setCurrentIdStore({}));
    });

    it('setPersonDataStore', () => {
        assert.deepEqual(actions.setPersonDataStore({}), mockActions.setPersonDataStore({}));
    });

    it('changeModalStatusStore', () => {
        assert.deepEqual(actions.changeModalStatusStore({}), mockActions.changeModalStatusStore({}));
    });

    it('setOfCurrentKeyStore', () => {
        assert.deepEqual(actions.setOfCurrentKeyStore({}), mockActions.setOfCurrentKeyStore({}));
    });

    it('setOfIdStore', () => {
        assert.deepEqual(actions.setOfIdStore({}), mockActions.setOfIdStore({}));
    });
});