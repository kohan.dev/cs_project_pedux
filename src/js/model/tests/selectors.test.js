import * as selectors from '../selectors';
import * as mockSelectors from '../../../testUtils/mockData/mockSelectors';

describe('Test modelSelectors', () => {
    const state = store.getState();

    it('getCurrentUuid', () => {
        assert.deepEqual(selectors.getCurrentUuid(state), mockSelectors.getCurrentUuid(state));
    });

    it('getCurrentVersion', () => {
        assert.deepEqual(selectors.getCurrentVersion(state), mockSelectors.getCurrentVersion(state));
    });

    it('getSelectedTab', () => {
        assert.deepEqual(selectors.getSelectedTab(state), mockSelectors.getSelectedTab(state));
    });

    it('getLoginData', () => {
        assert.deepEqual(selectors.getLoginData(state), mockSelectors.getLoginData(state));
    });

    it('getCurrentId', () => {
        assert.deepEqual(selectors.getCurrentId(state), mockSelectors.getCurrentId(state));
    });

    it('getPerson', () => {
        assert.deepEqual(selectors.getPerson(state), mockSelectors.getPerson(state));
    });

    it('getCurrentUuid', () => {
        assert.deepEqual(selectors.getCurrentUuid(state), mockSelectors.getCurrentUuid(state));
    });

    it('getUser', () => {
        assert.deepEqual(selectors.getUser(state), mockSelectors.getUser(state));
    });

    it('getSetOfVersion', () => {
        assert.deepEqual(selectors.getSetOfVersion(state), mockSelectors.getSetOfVersion(state));
    });

    it('getSetOfId', () => {
        assert.deepEqual(selectors.getSetOfId(state), mockSelectors.getSetOfId(state));
    });

    it('getCurrentKeys', () => {
        assert.deepEqual(selectors.getCurrentKeys(state), mockSelectors.getCurrentKeys(state));
    });

    it('getSearchKey', () => {
        assert.deepEqual(selectors.getSearchKey(state), mockSelectors.getSearchKey(state));
    });

    it('getChangeKey', () => {
        assert.deepEqual(selectors.getChangeKey(state), mockSelectors.getChangeKey(state));
    });

    it('getDeleteKey', () => {
        assert.deepEqual(selectors.getDeleteKey(state), mockSelectors.getDeleteKey(state));
    });
});
