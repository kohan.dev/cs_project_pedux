import reducer from '../redusers/settingsReducer';
import * as actions from '../../../testUtils/mockData/mockActions';

describe('Test settingsReducer', () => {
    it('default', () => {
        const state = {};
        assert.deepStrictEqual(reducer(state, {}), state);
    });

    it('CHANGE_SELECTED_TAB_STORE', () => {
        const state = {};

        const action = actions.changeSelectedTabStore({});

        assert.deepStrictEqual(reducer(state, action), state);
    });

    it('SET_SELECT_EDIT_STORE', () => {
        const state = {};

        const action = actions.setSelectEditStore({});

        assert.deepStrictEqual(reducer(state, action), state);
    });

    it('SET_OF_CURRENT_KEY_STORE', () => {
        const state = {};

        const action = actions.setOfCurrentKeyStore({});

        assert.deepStrictEqual(reducer(state, action), state);
    });
});
