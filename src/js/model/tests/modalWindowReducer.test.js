import reducer from '../redusers/modalWindowReducer';
import * as actions from '../../../testUtils/mockData/mockActions';

describe('Test modalWindowReducer', () => {
    it('default', () => {
        const state = {};
        assert.deepStrictEqual(reducer(state, {}), state);
    });

    it('CHANGE_MODAL_STATUS_STORE', () => {
        const state = {};

        const action = actions.changeModalStatusStore({});

        assert.deepStrictEqual(reducer(state, action), state);
    });
});
