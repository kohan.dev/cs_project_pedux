import * as mockActions from '../../../testUtils/mockData/mockActions';
import watchModelSaga, * as saga from '../model';
import {
    takeEvery,
} from 'redux-saga/effects';

describe('Test saga', () => {
    let sandbox = null;

    before(() => {
        sandbox = sinon.createSandbox();
    });

    after(() => {
        sandbox.restore();
    });

    describe('watchTestSaga', () => {
        const generator = watchModelSaga();

        it('should takeEvery CHANGE_AUTH_STATE', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.changeAuthState().type, saga.changeAuthState)
            );
        });

        it('should takeEvery SET_ERROR', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.setError().type, saga.setError)
            );
        });

        it('should takeEvery SET_USER_PERMISSIONS', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.setUserPermissions().type, saga.setUserPermissions)
            );
        });

        it('should takeEvery SET_CURRENT_UUID', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.setCurrentUuid().type, saga.setCurrentUuid)
            );
        });

        it('should takeEvery CHANGE_CURRENT_VERSION', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.changeCurrentVersion().type, saga.changeCurrentVersion)
            );
        });

        it('should takeEvery CHANGE_LIST_OF_PERSONS', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.changeListOfPersons().type, saga.changeListOfPersons)
            );
        });

        it('should takeEvery CHANGE_LOGS', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.changeLogs().type, saga.changeLogs)
            );
        });

        it('should takeEvery CHANGE_SELECTED_TAB', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.changeSelectedTab().type, saga.changeSelectedTab)
            );
        });

        it('should takeEvery SET_PERSON_DATA', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.setPersonData().type, saga.setPersonData)
            );
        });

        it('should takeEvery SAVE_CHANGE_EDIT_FORM_SAGA', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.savePersonData().type, saga.savePersonData)
            );
        });

        it('should takeEvery MERGE_CHANGE_EDIT_FORM_SAGA', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.mergePersonData().type, saga.mergePersonData)
            );
        });

        it('should takeEvery LOGOUT', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.logout().type, saga.logout)
            );
        });

        it('should takeEvery CHANGE_MODAL_STATUS', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.changeModalStatus().type, saga.changeModalStatus)
            );
        });

        it('should takeEvery SET_OF_ID', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.setOfId().type, saga.setOfId)
            );
        });

        it('should takeEvery SET_OF_CURRENT_KEY', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.setCurrentKey().type, saga.setCurrentKey)
            );
        });

        it('should takeEvery ADD_NEW_KEY', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.addNewKey().type, saga.addNewKey)
            );
        });

        it('should takeEvery ADD_NEW_KEY_ALL', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.addNewKeyAll().type, saga.addNewKeyAll)
            );
        });

        it('should takeEvery CHANGE_KEY_CURRENT', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.changeKeyCurrent().type, saga.changeKeyCurrent)
            );
        });

        it('should takeEvery CHANGE_KEY_ALL', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.changeKeyAll().type, saga.changeKeyAll)
            );
        });

        it('should takeEvery DELETE_KEY_CURRENT', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.deleteKeyCurrent().type, saga.deleteKeyCurrent)
            );
        });

        it('should takeEvery DELETE_KEY_ALL', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.deleteKeyAll().type, saga.deleteKeyAll)
            );
        });

        it('should be finished', () => {
            assert.isTrue(generator.next().done);
        });
    });

    // describe('testAction', () => {
    //     const action = mockActions.testAction({});
    //     const generator = saga.testAction(action);
    //
    //     it('should be finished', () => {
    //         const stub = sandbox.stub(console, 'log');
    //
    //         assert.isTrue(generator.next().done);
    //         sandbox.assert.calledOnce(stub);
    //     });
    // });
});
