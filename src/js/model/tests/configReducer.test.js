import reducer from '../redusers/configReducer';
import * as actions from '../../../testUtils/mockData/mockActions';

describe('Test configReducer', () => {
    it('default', () => {
        const state = {};
        assert.deepStrictEqual(reducer(state, {}), state);
    });

    it('CHANGE_CURRENT_VERSION_STORE', () => {
        const state = {};

        const action = actions.changeCurrentVersionStore({});

        assert.deepStrictEqual(reducer(state, action), state);
    });
});
