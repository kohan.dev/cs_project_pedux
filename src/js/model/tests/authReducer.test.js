import reducer from '../redusers/authReducer';
import * as actions from '../../../testUtils/mockData/mockActions';

describe('Test authReducer', () => {
    it('default', () => {
        const state = {};
        assert.deepStrictEqual(reducer(state, {}), state);
    });

    it('CHANGE_LOGIN_DATA_STORE', () => {
        const state = { loginData: {} };

        const action = actions.changeLoginDataStore({});

        assert.deepStrictEqual(reducer(state, action), state);
    });

    it('SET_ERROR_STORE', () => {
        const state = { errorData: {} };

        const action = actions.setErrorStore({});

        assert.deepStrictEqual(reducer(state, action), state);
    });

    it('SET_CURRENT_UUID_STORE', () => {
        const state = {};

        const action = actions.setCurrentUuidStore({});

        assert.deepStrictEqual(reducer(state, action), state);
    });

    it('SET_USER_PERMISSIONS_STORE', () => {
        const state = { userPermissions: {} };

        const action = actions.setUserPermissionsStore({});

        assert.deepStrictEqual(reducer(state, action), state);
    });

    it('CHANGE_AUTH_STATE_STORE', () => {
        const state = {};

        const action = actions.changeAuthStateStore({});

        assert.deepStrictEqual(reducer(state, action), state);
    });
});
