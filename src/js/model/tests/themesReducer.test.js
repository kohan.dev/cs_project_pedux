import reducer from '../redusers/themesReducer';

describe('Test themesReducer', () => {
    it('default', () => {
        const state = {};
        assert.deepStrictEqual(reducer(state, {}), state);
    });
});
