import reducer from '../redusers/modelReducer';
import * as actions from '../../../testUtils/mockData/mockActions';

describe('Test modelReducer', () => {
    it('default', () => {
        const state = {};
        assert.deepStrictEqual(reducer(state, {}), state);
    });

    it('CHANGE_LIST_OF_PERSONS_STORE', () => {
        const state = {};

        const action = actions.changeListOfPersonsStore({});

        assert.deepStrictEqual(reducer(state, action), state);
    });

    it('CHANGE_LOGS_STORE', () => {
        const state = {};

        const action = actions.changeLogsStore({});

        assert.deepStrictEqual(reducer(state, action), state);
    });

    it('SET_CURRENT_ID_STORE', () => {
        const state = {};

        const action = actions.setCurrentIdStore({});

        assert.deepStrictEqual(reducer(state, action), state);
    });

    it('SET_PERSON_DATA_STORE', () => {
        const state = {};

        const action = actions.setPersonDataStore({});

        assert.deepStrictEqual(reducer(state, action), state);
    });

    it('CHANGE_PERSON_DATA_STORE', () => {
        const state = { person: {} };

        const action = actions.changePersonDataStore({});

        assert.deepStrictEqual(reducer(state, action), state);
    });

    it('SET_OF_ID_STORE', () => {
        const state = {};

        const action = actions.setOfIdStore({});

        assert.deepStrictEqual(reducer(state, action), state);
    });

    it('INPUT_ADD_CHANGE', () => {
        const state = {};

        const action = actions.inputAddChange({});

        assert.deepStrictEqual(reducer(state, action), state);
    });

    it('INPUT_EDIT_CHANGE', () => {
        const state = {};

        const action = actions.inputEditChange({});

        assert.deepStrictEqual(reducer(state, action), state);
    });

    it('ADD_CHANGE_KEY', () => {
        const state = {};

        const action = actions.handleChangeKey({});

        assert.deepStrictEqual(reducer(state, action), state);
    });

    it('ADD_DELETE_KEY', () => {
        const state = {};

        const action = actions.handleDeleteKey({});

        assert.deepStrictEqual(reducer(state, action), state);
    });

    it('DELETE_KEY_CURRENT', () => {
        const state = {};

        const action = actions.deleteKeyCurrent({});

        assert.deepStrictEqual(reducer(state, action), state);
    });
});
