import { mapStateToProps } from '../index.js';
import * as mockSelectors from '../../../../testUtils/mockData/mockSelectors';
import * as selectors from '../selectors';

// const Chart = new Index().args; // TODO for UI

const currentState = store.getState();

describe('HOC MainModule', () => {
    let sandbox;

    before(() => {
        sandbox = sinon.createSandbox();
    });

    after(() => {
        sandbox.restore();
    });

    it('mapStateToProps should return correct selectors', () => {
        const mockMapStateToProps = state => ({
            theme: selectors.getMainModuleTheme(state),
        });
        const expected = mockMapStateToProps(currentState);
        const actual = mapStateToProps(currentState);
        assert.deepEqual(actual, expected);
    });
});
