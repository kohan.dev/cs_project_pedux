import * as selectors from '../selectors';
import * as mockSelectors from '../../../../testUtils/mockData/mockSelectors';

describe('Test MainModuleSelectors', () => {
    const state = store.getState();

    it('getMainModuleTheme', () => {
        assert.deepEqual(selectors.getMainModuleTheme(state), mockSelectors.getMainModuleTheme(state));
    });
});
