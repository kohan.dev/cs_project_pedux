import {
    MainContainer,
} from '../styledComponents';

describe('MainModule styled components', () => {
    const theme = {
        containerWidth: 'containerWidth',
        borderRadius: 'borderRadius',
        bgColor: 'bgColor',
        borderColor: 'borderColor',
    };

    it('MainContainer should have correct styles', () => {
        const component = shallowRender(<MainContainer theme={theme} />);

        expect(component).toHaveStyleRule('width', 'containerWidth');
        expect(component).toHaveStyleRule('background-color', 'bgColor');
        expect(component).toHaveStyleRule('border', '1px solid borderColor');
        expect(component).toHaveStyleRule('border-radius', 'borderRadius');
    });
});
