import * as selectors from '../selectors';
import * as mockSelectors from '../../../../testUtils/mockData/mockSelectors';

describe('Test LoginModuleSelectors', () => {
    const state = store.getState();

    it('getLoginModuleTheme', () => {
        assert.deepEqual(selectors.getLoginModuleTheme(state), mockSelectors.getLoginModuleTheme(state));
    });

    it('getErrorData', () => {
        assert.deepEqual(selectors.getErrorData(state), mockSelectors.getErrorData(state));
    });

    it('getLoginData', () => {
        assert.deepEqual(selectors.getLoginData(state), mockSelectors.getLoginData(state));
    });
});
