import * as actions from '../actions';
import * as mockActions from '../../../../testUtils/mockData/mockActions';

describe('Test LoginModuleActions', () => {
    it('changeLoginData', () => {
        assert.deepEqual(actions.changeLoginData({}), mockActions.changeLoginData({}));
    });

    it('changeAuthState', () => {
        assert.deepEqual(actions.changeAuthState({}), mockActions.changeAuthState({}));
    });
});
