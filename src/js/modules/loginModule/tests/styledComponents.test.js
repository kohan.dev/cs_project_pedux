import {
    Container,
    FormItem,
    FormButton,
} from '../styledComponents';

describe('LoginModule styled components', () => {
    const theme = {
        containerWidth: 'containerWidth',
        borderRadius: 'borderRadius',
        font: 'font',
        bgColor: 'bgColor',
        borderColor: 'borderColor',
        textColor: 'textColor',
        inputBgColor: 'inputBgColor',
        buttonBorderColor: 'buttonBorderColor',
        buttonBgColor: 'buttonBgColor',
        buttonTextColor: 'buttonTextColor',
    };

    it('Container should have correct styles', () => {
        const component = shallowRender(<Container theme={theme} />);

        expect(component).toHaveStyleRule('width', 'containerWidth');
        expect(component).toHaveStyleRule('background-color', 'bgColor');
        expect(component).toHaveStyleRule('border', '1px solid borderColor');
        expect(component).toHaveStyleRule('border-radius', 'borderRadius');
        expect(component).toHaveStyleRule('font-family', 'font');
        expect(component).toHaveStyleRule('color', 'textColor');
    });

    it('FormItem.input should have correct styles', () => {
        const component = shallowRender(<FormItem.input theme={theme} />);

        expect(component).toHaveStyleRule('color', 'textColor');
        expect(component).toHaveStyleRule('border', '1px solid borderColor');
        expect(component).toHaveStyleRule('background-color', 'inputBgColor');
        expect(component).toHaveStyleRule('border-radius', 'borderRadius');
    });

    it('FormItem.label should have correct styles', () => {
        const component = shallowRender(<FormItem.label theme={theme} />);

        expect(component).toHaveStyleRule('font-family', 'font');
    });

    it('FormButton.button should have correct styles', () => {
        const component = shallowRender(<FormButton.button theme={theme} />);

        expect(component).toHaveStyleRule('border-radius', 'borderRadius');
        expect(component).toHaveStyleRule('border', '1px solid buttonBorderColor');
        expect(component).toHaveStyleRule('background-color', 'buttonBgColor');
        expect(component).toHaveStyleRule('color', 'buttonTextColor');
        expect(component).toHaveStyleRule('font-family', 'font');
    });
});
