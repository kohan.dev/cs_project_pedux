export const getLoginData = state => state.authData.loginData;
export const getErrorData = state => state.authData.errorData;
export const getLoginModuleTheme = state => state.themes.theme.loginModule;

