import * as selectors from '../selectors';
import * as mockSelectors from '../../../../testUtils/mockData/mockSelectors';

describe('Test RootModuleSelectors', () => {
    const state = store.getState();

    it('getRootModuleTheme', () => {
        assert.deepEqual(selectors.getRootModuleTheme(state), mockSelectors.getRootModuleTheme(state));
    });

    it('getAuthState', () => {
        assert.deepEqual(selectors.getAuthState(state), mockSelectors.getAuthState(state));
    });
});
