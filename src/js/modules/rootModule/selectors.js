export const getRootModuleTheme = state => state.themes.theme.rootModule;
export const getAuthState = state => state.authData.isLoggedSuccessfully;
